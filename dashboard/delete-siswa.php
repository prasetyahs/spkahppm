<?php

include '../config/database.php';
include '../config/functions.php';

$type = $_GET['type'];
switch ($type) {
    case 'jurusan':
        # code...
        $id = $_GET['id_jurusan'];
        delete("jurusan", ['id_jurusan' => $id], $conn);
        Redirect($baseUrl . "dashboard/data-jurusan.php");
        break;
    case 'kriteria':
        # code...
        $id = $_GET['id_criteria'];
        delete("kriteria", ['id_criteria' => $id], $conn);
        Redirect($baseUrl . "dashboard/data-kriteria.php");
        break;
    case 'sub_criteria':
        # code...
        $id = $_GET['id_sub_criteria'];
        delete("sub_kriteria", ['id_sub_criteria' => $id], $conn);
        Redirect($baseUrl . "dashboard/data-sub-kriteria.php");
        break;

    default:
        # code...
        $id = $_GET['nisn'];
        delete("siswa", ['nisn' => $id], $conn);
        Redirect($baseUrl . "dashboard/data-siswa.php");
        break;
}
