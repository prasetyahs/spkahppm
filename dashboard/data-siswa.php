<?php
include '../layout/header.php';
if ($_SESSION['login'] != true) {
    header("Location: " . $baseUrl);
    exit();
}
include '../layout/navbar.php';
include '../layout/sidebar.php';
if (isset($_POST['submit_update'])) {
    update($_POST, ["nisn" => $_POST['nisn']], "siswa", $conn);
}

if (isset($_POST['submit_add'])) {
    $nisn = $_POST['nisn'];
    if (!empty(readDataPerRow($conn, "SELECT * from siswa where nisn= '$nisn'"))) {
        $_SESSION['message'] = "Siswa sudah terdata";
        $_SESSION['type'] = "warning";
        $_SESSION['title'] = "Warning";
    } else {
        create($_POST, $conn, "siswa");
        $_SESSION['message'] = "Siswa didata";
        $_SESSION['type'] = "success";
        $_SESSION['title'] = "Success";
    }
}
$data = readDataAllRow($conn, "SELECT * FROM siswa");

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Siswa</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Data Siswa</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Siswa</h3>
                                <button class="btn btn-outline-primary" data-toggle="modal" data-target="#modalAdd" onclick="addData()" style="float: right;"><i class="fa fa-plus"></i> Tambah</button>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NISN</th>
                                            <th>Nama Siswa</th>
                                            <th>Asal Sekolah</th>
                                            <th>No. Telepon</th>
                                            <th>Alamat</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1;
                                        foreach ($data as $d) { ?>
                                            <tr>
                                                <td><?= $i ?>.</td>
                                                <td><?= $d['nisn'] ?></td>
                                                <td><?= $d['name'] ?></td>
                                                <td><?= $d['school_origin'] ?></td>
                                                <td><?= $d['number_phone'] ?></td>
                                                <td><?= $d['address'] ?></td>
                                                <td><?= $d['gender'] === '1' ? "Perempuan" : "Laki-laki" ?></td>
                                                <td>
                                                    <button data-toggle="modal" data-target="#modalAdd" class="btn btn-transparent p-0" onclick="updateData('<?= $d['nisn'] ?>','<?= $d['name'] ?>','<?= $d['school_origin'] ?>','<?= $d['number_phone'] ?>','<?= $d['address'] ?>','<?= $d['gender'] ?>')"> <i class="fas fa-edit text-primary"></i></button>
                                                    <a href="delete-siswa.php?nisn=<?= $d['nisn'] ?>" class="btn btn-transparent p-0"> <i class="fas fa-trash text-delete text-danger"></i></a>
                                                </td>

                                            </tr>
                                        <?php $i++;
                                        } ?>
                                    </tbody>


                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </section>

        <div class="modal fade" id="modalAdd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="form-group row">
                                <label for="" class="col-sm-2">NISN</label>
                                <div class="col-sm-10">
                                    <input type="number" required name="nisn" id="nisn" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2">Nama Siswa</label>
                                <div class="col-sm-10">
                                    <input type="text" required name="name" id="name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2">Jenis Kelamin</label>
                                <div class="form-check pr-2">
                                    <input class="form-check-input" name="gender" type="radio" value="0" checked name="flexRadioDefault" id="gender">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        Laki-Laki
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" value="1" name="flexRadioDefault" id="gender">
                                    <label class="form-check-label" for="flexRadioDefault2">
                                        Perempuan
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2">Asal Sekolah</label>
                                <div class="col-sm-10">
                                    <input type="text" id="school_origin" name="school_origin" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2">No. Telepon</label>
                                <div class="col-sm-10">
                                    <input type="tel" id="number_phone" name="number_phone" pattern="[+]{1}[0-9]{11,14}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2">Alamat</label>
                                <div class="col-sm-10">
                                    <textarea name="address" id="address" cols="30" rows="2" class="form-control"></textarea>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit_add" class="btn btn-primary" id="btn_form">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalDelete" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title">Form Hapus Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Anda yakin ingin menghapus data ?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a id="btn_delete" class="btn btn-primary">Hapus Data</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    function addData() {
        document.getElementById('modal-title').innerHTML = "Form Tambah Data";
        document.getElementById('btn_form').innerHTML = "Tambah Data";
        document.getElementById('id_number').value = "";
        document.getElementById('name').value = "";
        document.getElementById('role').value = "";
        $('#btn_form').attr('name', 'submit_add');

        document.getElementById('id_number').readOnly = false;
    }

    function updateData(nisn, name, origin, phone, address, gender) {
        console.log(nisn)
        document.getElementById('modal-title').innerHTML = "Form Update Data";
        document.getElementById('btn_form').innerHTML = "Perbarui Data";
        document.getElementById('nisn').value = nisn;
        document.getElementById('name').value = name;
        document.getElementById('school_origin').value = origin;
        document.getElementById('number_phone').value = phone;
        document.getElementById('address').value = address;
        document.getElementById('gender') = gender;
        $('#btn_form').attr('name', 'submit_update');
        document.getElementById('nisn').readOnly = true;
    }

    function deleteData(id, baseUrl) {
        document.getElementById('btn_delete').href = baseUrl + 'dashboard/delete-users.php?id=' + id;
    }
</script>

<?php
include '../layout/footer.php';
?>