<?php
include '../layout/header.php';
if ($_SESSION['login'] != true) {
    header("Location: " . $baseUrl);
    exit();
}
include '../layout/navbar.php';
include '../layout/sidebar.php';
if (isset($_POST['submit_update'])) {
    update($_POST, ["id_sub_criteria" => $_POST['id_sub_criteria']], "sub_kriteria", $conn);
    $_SESSION['message'] = "Berhasil Update Jurusan";
    $_SESSION['type'] = "success";
    $_SESSION['title'] = "Success";
}

if (isset($_POST['submit_add'])) {
    $id_sub_criteria = $_POST['id_sub_criteria'];
    if (!empty(readDataPerRow($conn, "SELECT * from sub_kriteria where id_sub_criteria= '$id_sub_criteria'"))) {
        $_SESSION['message'] = "kode sub kriteria sudah terdata";
        $_SESSION['type'] = "warning";
        $_SESSION['title'] = "Warning";
    } else {

        create($_POST, $conn, "sub_kriteria");
        $_SESSION['message'] = "Berhasil Submit sub kriteria";
        $_SESSION['type'] = "success";
        $_SESSION['title'] = "Success";
    }
}
$data = readDataAllRow($conn, "SELECT * FROM sub_kriteria");
$kriteria = readDataAllRow($conn, "SELECT * FROM kriteria");
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Siswa</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Data Siswa</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Siswa</h3>
                                <button class="btn btn-outline-primary" data-toggle="modal" data-target="#modalAdd" onclick="addData()" style="float: right;"><i class="fa fa-plus"></i> Tambah</button>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID Sub kriteria</th>
                                            <th>Nama Sub kriteria</th>
                                            <th>ID kriteria</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1;
                                        foreach ($data as $d) { ?>
                                            <tr>
                                                <td><?= $i ?>.</td>
                                                <td><?= $d['id_sub_criteria'] ?></td>
                                                <td><?= $d['sub_criteria_name'] ?></td>
                                                <td><?= $d['id_criteria'] ?></td>
                                                <td>
                                                    <button data-toggle="modal" data-target="#modalAdd" class="btn btn-transparent p-0" onclick="updateData(`<?= $d['id_sub_criteria'] ?>`,`<?= $d['sub_criteria_name'] ?>`,`<?= $d['id_criteria'] ?>`)"> <i class="fas fa-edit text-primary"></i></button>
                                                    <a href="delete-siswa.php?type=sub_criteria&id_sub_criteria=<?= $d['id_sub_criteria'] ?>" class="btn btn-transparent p-0"> <i class="fas fa-trash text-delete text-danger"></i></a>
                                                </td>

                                            </tr>
                                        <?php $i++;
                                        } ?>
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </section>

        <div class="modal fade" id="modalAdd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="form-group row">
                                <label for="" class="col-sm-2">ID Sub Criteria</label>
                                <div class="col-sm-10">
                                    <input type="text" required name="id_sub_criteria" id="id_sub_criteria" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2">Sub Criteria Name</label>
                                <div class="col-sm-10">
                                    <input type="text" required name="sub_criteria_name" id="sub_criteria_name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2">kriteria</label>
                                <div class="col-sm-10">
                                    <select name="id_criteria" id="id_criteria" class="form-control">
                                        <option value="">-- Pilih Kriteria --</option>
                                        <?php $i = 1;
                                        foreach ($kriteria as $d) { ?>
                                            <option value="<?= $d['id_criteria'] ?>"><?= $d['criteria_name'] ?></option>
                                        <?php $i++;
                                        } ?>
                                    </select>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" class="btn btn-primary" id="btn_form">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalDelete" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title">Form Hapus Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Anda yakin ingin menghapus data ?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a id="btn_delete" class="btn btn-primary">Hapus Data</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    function addData() {
        document.getElementById('modal-title').innerHTML = "Form Tambah Data";
        document.getElementById('btn_form').innerHTML = "Tambah Data";
        document.getElementById('id_sub_criteria').value = "";
        document.getElementById('id_criteria').value = "";
        document.getElementById('sub_criteria_name').value = "";
        $('#btn_form').attr('name', 'submit_add');

        document.getElementById('id_sub_criteria').readOnly = false;
    }

    function updateData(id, name, role) {
        document.getElementById('modal-title').innerHTML = "Form Update Data";
        document.getElementById('btn_form').innerHTML = "Perbarui Data";
        document.getElementById('id_sub_criteria').value = id;
        document.getElementById('id_criteria').value = role;
        document.getElementById('sub_criteria_name').value = name;
        $('#btn_form').attr('name', 'submit_update');
        document.getElementById('id_sub_criteria').readOnly = true;
    }

    function deleteData(id, baseUrl) {
        document.getElementById('btn_delete').href = baseUrl + 'dashboard/delete-users.php?id=' + id;
    }
</script>

<?php
include '../layout/footer.php';
?>