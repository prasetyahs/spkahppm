<?php
include '../layout/header.php';
if ($_SESSION['login'] != true) {
    header("Location: " . $baseUrl);
    exit();
}
include '../layout/navbar.php';
include '../layout/sidebar.php';
if (isset($_POST['submit_update'])) {
    update($_POST, ["id_criteria" => $_POST['id_criteria']], "kriteria", $conn);
    $_SESSION['message'] = "Berhasil Update Jurusan";
    $_SESSION['type'] = "success";
    $_SESSION['title'] = "Success";
}

if (isset($_POST['submit_add'])) {
    $id_criteria = $_POST['id_criteria'];
    if (!empty(readDataPerRow($conn, "SELECT * from kriteria where id_criteria= '$id_criteria'"))) {
        $_SESSION['message'] = "kode kriteria sudah terdata";
        $_SESSION['type'] = "warning";
        $_SESSION['title'] = "Warning";
    } else {

        create($_POST, $conn, "kriteria");
        $_SESSION['message'] = "Berhasil Submit kriteria";
        $_SESSION['type'] = "success";
        $_SESSION['title'] = "Success";
    }
}
$data = readDataAllRow($conn, "SELECT * FROM kriteria");
?>

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Siswa</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Data Siswa</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Siswa</h3>
                                <button class="btn btn-outline-primary" data-toggle="modal" data-target="#modalAdd" onclick="addData()" style="float: right;"><i class="fa fa-plus"></i> Tambah</button>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID Kriteria</th>
                                            <th>Nama Kriteria</th>
                                            <th>Nilai Eigenvector</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i = 1;
                                        foreach ($data as $d) { ?>
                                            <tr>
                                                <td><?= $i ?>.</td>
                                                <td><?= $d['id_criteria'] ?></td>
                                                <td><?= $d['criteria_name'] ?></td>
                                                <td><?= $d['nilai_eigenvector'] ?></td>
                                                <td>
                                                    <button data-toggle="modal" data-target="#modalAdd" class="btn btn-transparent p-0" onclick="updateData(`<?= $d['id_criteria'] ?>`,`<?= $d['criteria_name'] ?>`,`<?= $d['nilai_eigenvector'] ?>`)"> <i class="fas fa-edit text-primary"></i></button>
                                                    <a href="delete-siswa.php?type=kriteria&id_criteria=<?= $d['id_criteria'] ?>" class="btn btn-transparent p-0"> <i class="fas fa-trash text-delete text-danger"></i></a>
                                                </td>

                                            </tr>
                                        <?php $i++;
                                        } ?>
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </section>

        <div class="modal fade" id="modalAdd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="form-group row">
                                <label for="" class="col-sm-2">ID Kriteria</label>
                                <div class="col-sm-10">
                                    <input type="text" required name="id_criteria" id="id_criteria" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" required name="criteria_name" id="criteria_name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2">Nilai Eigenvector</label>
                                <div class="col-sm-10">
                                    <input type="text" required name="nilai_eigenvector" id="nilai_eigenvector" class="form-control">
                                </div>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btn_form">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalDelete" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title">Form Hapus Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Anda yakin ingin menghapus data ?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a id="btn_delete" class="btn btn-primary">Hapus Data</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    function addData() {
        document.getElementById('modal-title').innerHTML = "Form Tambah Data";
        document.getElementById('btn_form').innerHTML = "Tambah Data";
        document.getElementById('id_criteria').value = "";
        document.getElementById('criteria_name').value = "";
        document.getElementById('nilai_eigenvector').value = "";
        $('#btn_form').attr('name', 'submit_add');

        document.getElementById('id_criteria').readOnly = false;
    }

    function updateData(id, name, role) {
        document.getElementById('modal-title').innerHTML = "Form Update Data";
        document.getElementById('btn_form').innerHTML = "Perbarui Data";
        document.getElementById('id_criteria').value = id;
        document.getElementById('criteria_name').value = name;
        document.getElementById('nilai_eigenvector').value = role;
        $('#btn_form').attr('name', 'submit_update');
        document.getElementById('id_criteria').readOnly = true;
    }

    function deleteData(id, baseUrl) {
        document.getElementById('btn_delete').href = baseUrl + 'dashboard/delete-users.php?id=' + id;
    }
</script>

<?php
include '../layout/footer.php';
?>